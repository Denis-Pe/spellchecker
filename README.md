# spellchecker

Rust library with functionality to spellcheck text.

This crate is made with a focus in being able to work with as many languages as possible, hence it **always** uses graphemes instead of Rust `char`s or individual bytes.

## Examples

```rust
use spellchecker::levenshtein_slo;

// levenshtein_slo is very slow. To figure out
// this 1 difference between *did* and *dad*, it went
// through 7 runs.
assert_eq!(levenshtein_slo("did", "dad"), 1);

// The performance deteriorates very quickly.
// The following calls go through
// 29, 382, 74693 runs, respectively.
levenshtein_slo("adv", "una");
levenshtein_slo("advan", "unadv");
levenshtein_slo("advanced", "unadvanced");

// To compare a block of text against a glossary of words,
// use `spellcheck_slo`, which returns a vector of `Change`s
// with all `possible_changes` being glossary words whose Levenshtein Edit Distance
// is less than, or equal to, 2.
// It is slow because it uses `levenshtein_slo` as its backend.
assert_eq!(
    spellcheck_slo("cel ohone numner", &vec!["cell", "phone", "number"]),
    vec![
        Change {
            original_word: "cel",
            possible_replacements: vec!["cell"],
        },
        Change {
            original_word: "ohone",
            possible_replacements: vec!["phone"],
        },
        Change {
            original_word: "numner",
            possible_replacements: vec!["number"],
        },
    ]
);
```

## To-do

- [ ] Add more testing with whole alphabets, longer sentences
- [ ] Add faster algorithms, such as Wagner-Fischer.
- [ ] Add awareness of keyboard layout