/*!
 * A spellchecker library made with flexibility in mind.
 *
 * In this crate, there is a strict policy to work with graphemes all the time
 * and with no exception.
 * To see why this is the case, see what [The Book says about it](https://doc.rust-lang.org/book/ch08-02-strings.html#bytes-and-scalar-values-and-grapheme-clusters-oh-my).
 * This crate's aim is to work with as many languages as possible, hence working with bytes
 * or even `char`s is something that is not desirable since it would limit the functionality in this crate to a number of languages whose letters are always a single `char` or byte.
 *
 * # Examples
 *
 * ```
 * use spellchecker::levenshtein_slo;
 *
 * // levenshtein_slo is very slow. To figure out
 * // this 1 difference between *did* and *dad*, it went
 * // through 7 runs.
 * assert_eq!(levenshtein_slo("did", "dad"), 1);
 *
 * // The performance deteriorates very quickly.
 * // The following calls go through
 * // 29, 382, 74693 runs, respectively.
 * levenshtein_slo("adv", "una");
 * levenshtein_slo("advan", "unadv");
 * levenshtein_slo("advanced", "unadvanced");
 *
 * // To compare a block of text against a glossary of words,
 * // use `spellcheck_slo`, which returns a vector of `Change`s
 * // with all `possible_changes` being glossary words whose Levenshtein Edit Distance
 * // is less than, or equal to, 2.
 * // It is slow because it uses `levenshtein_slo` as its backend.
 * assert_eq!(
 *     spellcheck_slo("cel ohone numner", &vec!["cell", "phone", "number"]),
 *     vec![
 *         Change {
 *             original_word: "cel",
 *             possible_replacements: vec!["cell"],
 *         },
 *         Change {
 *             original_word: "ohone",
 *             possible_replacements: vec!["phone"],
 *         },
 *         Change {
 *             original_word: "numner",
 *             possible_replacements: vec!["number"],
 *         },
 *     ]
 * );
 * ```
 *
 * # Important Notes
 * This crate depends on [unicode-segmentation](https://crates.io/crates/unicode-segmentation) for its grapheme and word functionality. So keep in mind
 * that **grapheme** and **word** here are [as defined in the crate](https://unicode-rs.github.io/unicode-segmentation/unicode_segmentation/trait.UnicodeSegmentation.html).
 *
 * Also keep in mind that I am only proficient in English and Spanish,
 * so those are the two languages this crate is tested with.
 */
use unicode_segmentation::UnicodeSegmentation;

/// `struct` representing a `Change` that could be made
/// to a word.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Change<'text> {
    pub original_word: &'text str,
    pub possible_replacements: Vec<&'text str>,
}

/// Original, but slow and recursive, implementation of the
/// Levenshtein Edit Distance algorithm.
pub fn levenshtein_slo(a: &str, b: &str) -> usize {
    if a.len() == 0 || b.len() == 0 {
        return a.graphemes(true).count() + b.graphemes(true).count();
    }

    let firsta = a.graphemes(true).next().unwrap();
    let firstb = b.graphemes(true).next().unwrap();

    let resta = skip_graphemes(a, 1);
    let restb = skip_graphemes(b, 1);

    if firsta == firstb {
        levenshtein_slo(resta, restb)
    } else {
        1 + levenshtein_slo(resta, b)
            .min(levenshtein_slo(a, restb).min(levenshtein_slo(resta, restb)))
    }
}

/// Compare words in a block of text to a glossary of valid words
///
/// This function will return `Change`s whose possible_replacements
/// are glossary words that have a Levenshtein Edit Distance of 2 or less
/// between them and the original word, of course ignoring all words that
/// are already correct.
pub fn spellcheck_slo<'text, T: AsRef<str>>(
    text: &'text str,
    glossary: &'text [T],
) -> Vec<Change<'text>> {
    let words: Vec<&str> = text.unicode_words().collect();
    let mut output = Vec::new();

    for word in words {
        if glossary
            .iter()
            .any(|gloss_word| gloss_word.as_ref() == word)
        {
            continue;
        }

        output.push(
            glossary
                .iter()
                .map(|gloss_word| (gloss_word, levenshtein_slo(gloss_word.as_ref(), word)))
                .filter(|(_gloss_word, lev_result)| *lev_result <= 2)
                .fold(
                    Change {
                        original_word: word,
                        possible_replacements: Vec::new(),
                    },
                    |mut change, (gloss_word, score)| {
                        if score == 1 {
                            change.possible_replacements.insert(0, gloss_word.as_ref())
                        } else {
                            change.possible_replacements.push(gloss_word.as_ref())
                        };
                        change
                    },
                ),
        );
    }

    output
}

fn skip_graphemes(string: &str, ngraphs: usize) -> &str {
    &string[string
        .graphemes(true)
        .take(ngraphs)
        .fold(0, |acc, next| acc + next.len())..]
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::*;

    #[test]
    fn levenshtein_slo_test() {
        let test_subjects = HashMap::from([
            (("mum", "num"), 1usize),
            (("keyboard", "board"), 3),
            (("mr", "mrs"), 1),
            (("mister", "mistress"), 3),
            (("me", "my"), 1),
            (("test", "jest"), 1),
            (("dinosaur", "dinosaurio"), 2),
            (("español", "espanol"), 1),
            (("estación", "enumeración"), 5),
            (("cuenta", "count"), 3),
            (("analyzer", "analizador"), 4),
            (("lingüística", "leñguistica"), 4),
        ]);

        for ((stra, strb), dist) in test_subjects.iter() {
            assert_eq!(levenshtein_slo(stra, strb), *dist)
        }
    }

    #[test]
    fn spellcheck_slo_test() {
        assert_eq!(
            spellcheck_slo("cel ohone numner", &vec!["cell", "phone", "number"]),
            vec![
                Change {
                    original_word: "cel",
                    possible_replacements: vec!["cell",],
                },
                Change {
                    original_word: "ohone",
                    possible_replacements: vec!["phone",],
                },
                Change {
                    original_word: "numner",
                    possible_replacements: vec!["number",],
                },
            ]
        );
    }
}
